# Casimiro Pietro Ciancimino - WeRoad Home Assignment

## Usage

Assuming you have `node.js` and `npm` installed on your system for a smooth setup process, follow these steps:

1. **Clone the Repository**: First, clone the project repository from GitLab.
<br />
`https://gitlab.com/casiimir/travel-nuxt`
<br />

2. **Install Dependencies**: Navigate to the project directory and install the necessary dependencies.
<br />
`npm install`
<br />

3. **Run the Application**: Once the dependencies are installed, you can start the server.
<br />
`npm run dev`
<br />

This will launch the application locally on your machine.

## Design Assumptions

This micro-application encompasses two primary pages: 
1. **Travel Management (Main Page)**: Intended for agents to add, modify, or delete travel experiences.
2. **Bookings Management (`/booking`)**: Designed for users to make purchases of available experiences managed by agents.

## Scalable Architecture

- **Reusable Components**: Future expansion capabilities considered through the implementation of reusable components.
- **Tailwind CSS**: Utilized for structure, ensuring a consistent and scalable design.
- **Responsive Design**: Media queries for components, ready for inheritance and consistency across the application.
- **Routing and State Management**: Initially, no single source of truth for state management, but a potential future implementation with Pinia.
- **Data Handling**: A remote JSON SERVER (https://gitlab.com/casiimir/travel-json-server) set up for easy Rest API mode implementation.
- **CI/CD Pipeline**: Application follows a CI/CD pipeline, deployed on Vercel, and hosted on GitLab, facilitating efficient and well-prepared contributions.

## How to Use the Application

1. **Homepage/Agent Interface**: Allows viewing, editing, adding, and deleting travel experiences.
2. **Travel Cards**: Clicking on a card opens options for modification or deletion. A widget button at the bottom facilitates adding new experiences.
3. **Booking Process (`/booking`)**: User section for selecting and purchasing experiences via a guided wizard.
4. **Booking Management**: Non-persistent storage of bookings at the `/booking` page for scalability and presentation purposes. Personal user information, although not displayed, is captured client-side during the booking process.

## Improvements

1. **Global State Logic**: Migrating to a global state, introducing more specific categories, promotional banners, and marketing elements.
2. **Landing Page**: Incorporating all elements from the first point into a comprehensive landing page.
3. **User Section**: Enhancing user experience with personalized sections and information storage.
4. **UX Enhancements**: Introducing customized calendars, maps, and similar elements for a more engaging user experience.

## Architecture

- **Vue 3, Nuxt.js, Tailwind CSS, Figma, Lighthouse, TypeScript**: Core technologies and tools used in the project.
- **Component-Based Architecture**: Utilizing Vue's strengths for creating reusable UI components.
- **Server-Side Rendering**: Managed by Nuxt.js for improved SEO.
- **Responsive UI Design**: Developed in Figma for optimal device compatibility.
- **Performance Optimization**: Incorporating Nuxt/Image and similar tools for efficient loading.
- **Scalability**: Ready for increased feature and user load.
- **JSON Server Data Integration**: Remote JSON server for travel management, adaptable for external data sources like REST APIs.

---

For further details or contributions, please refer to the project repository on GitLab.
