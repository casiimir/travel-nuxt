export default defineNuxtConfig({
  devtools: { enabled: true },
  typescript: {
    typeCheck: true
  },
  modules: [
    '@nuxtjs/tailwindcss',
    ['@nuxtjs/robots', {
      UserAgent: '*',
      Disallow: ''
    }],
    '@pinia/nuxt',
    '@nuxt/image'
  ],
  app: {
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      title: 'Travel Nuxt',
      htmlAttrs: {
        lang: 'en'
      },
      meta: [
        {
          name: 'description',
          content: "Book Your Dream Trip Now! Immerse yourself in an unforgettable experience with our exclusive travel packages. Live the dream adventure you've always wanted!",
        },
      ]
    }
  },
  css: ['~/assets/css/main.css']
})
