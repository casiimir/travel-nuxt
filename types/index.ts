export interface Travel {
  id: number;
  destination: string;
  startDate: string;
  endDate: string;
  name: string;
  departureDate: string;
  returnDate: string;
  travelPeriod: string;
  totalDays: number;
  image: string;
  description: string;
  pricePerPerson: number;
  averageRating: number;
  user: UserData
}

export interface UserData {
  email?: string;
  name?: string;
  surname?: string;
  phone?: string;
  birthday?: string;
  paymentMethod?: string;
}
export interface BookingData {
  email?: string;
  name?: string;
  surname?: string;
  phone?: string;
  birthday?: string;
  paymentMethod?: string;
  travel?: Travel
}