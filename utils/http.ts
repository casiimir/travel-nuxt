import axios from 'axios';
import { type Travel } from '~/types';

const BASE_URL = 'https://travel-json-server-zd87.onrender.com/travels/'

/**
 * Performs an HTTP GET request.
 * @param id - The ID of the resource to retrieve (optional).
 * @returns A Promise that resolves to the response data.
 * @throws If an error occurs during the request.
 */
export const HTTP_GET = async (id: string = '') => {
  try {
    const response = await axios.get(`${BASE_URL}${id}`);
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

/**
 * Sends a POST request to the server with the provided data.
 * @param data The data to be sent in the request.
 * @returns A Promise that resolves to the response data from the server.
 * @throws If an error occurs during the request.
 */
export const HTTP_POST = async (data: Travel) => {
  try {
    const response = await axios.post(BASE_URL, data);
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

/**
 * Sends a PATCH request to update a resource with the specified ID.
 * 
 * @param {number} id - The ID of the resource to update.
 * @param {Travel} data - The data to be sent in the request body.
 * @returns {Promise<any>} - A promise that resolves to the response data.
 * @throws {Error} - If an error occurs during the request.
 */
export const HTTP_PATCH = async (id: number, data: Travel) => {
  try {
    const response = await axios.patch(`${BASE_URL}${id}`, data);
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

/**
 * Sends a DELETE request to the server using axios.
 * 
 * @param id - The ID of the resource to delete.
 * @returns A Promise that resolves to the response data from the server.
 * @throws If an error occurs during the request.
 */
export const HTTP_DELETE = async (id: number) => {
  try {
    const response = await axios.delete(`${BASE_URL}${id}`);
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}